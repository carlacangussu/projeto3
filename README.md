Aluna: Carla Rocha Cangussú  Matrícula: 17/0085023 Turma: A
Aluna: Giulia Alcantara   Matricula: 18/0121308  Turma: A


O que é?

O site de YourFanfic é uma aplicação web, isto é, um software desenvolvido para ser utilizado através de um navegador de internet. Este YourFanfic é um local onde os fãs encontram, e tem a possibilidade de criar, contos com seus ídolos, livros e filmes preferidos, conhecidos como fanfics ou somente fics.

Objetivos 
     Criar um fórum de fanfics, onde escritores amadores possam postar suas histórias ficcionais autorais de forma livre e informal. Nele, as pessoas podem ler as fanfics de seus temas favoritos e comentar opiniões, sugestões e mais o que quiserem em cada uma delas.

Funcionalidades

	- Cadastro de usuario(Adm e autor)
	- Controle de acesso
	- Publicação de Fanfics (Posts)
	- Busca por título e gênero
	- Ordenar por ordem alfabética os posts ao clicar no link título
	- Envio de fanfics
	- Comentar


Pendências

	- O presente projeto apresenta 4 models: user, post, comentario e fanfic. Uma fanfic pertence a um user, este poderá possuir mais de uma fanfic. Já o post pertence ao admin, mas pode receber comentários de outros user. A model comentario pertence a model post.
	- O controle de acesso pe feito pela diferenciação dos níveis de permissões para diferentes tipos de usuários. O usuário público(não cadastrado) pode visualizar todas as postagens do site. O usuário cadastrado regular pode  visualizar todas as postagens, comentar, pode criar/enviar sua fanfic. O usuário administrador tem todas as permissões do usuário cadastrado regular, mas também pode criar, editar e apagar posts, além de apagar comentários. Para acessar a aplicação com permissões de administrador, utilize o email "carla@oo.com" e a senha "testando".
	- As gems ultizadas além do padrão rails foram: devise e ransack.
Na elaboração da inteface utilizou-se bootstrap, html e CSS.
Os diagramas UML encontram-se na pasta diagramas.



Versão do ruby:    ruby 2.6.5p114 (2019-10-01 revision 67812) [x86_64-linux]

Versão do rais:      Rails 6.0.1

Para compilar siga os passos:
	- Abra o terminal e clone o repositório: git clone https://gitlab.com/carlacangussu/projeto3.git
 	- Entre no diretório em que se encontram os arquivos
	- Tenha as dependencias em suas versões corretas instaladas
	- Execute em sequencia os seguintes comando:
		bundle install
		rails db:migrate
		bundle exec puma -C config/puma.rb -b tcp://127.0.0.1:3001

No navegador, vá para "//localhost:3001"

Obs: No final do trabalho acabei tendo problema ao excutar o rails e como solução encontrei essa forma de inicializar o servidor

