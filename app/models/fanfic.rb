class Fanfic < ApplicationRecord
  belongs_to :user
  validates :titulo, :presence => true
  validates :genero, :presence => true
  validates :resumo, :presence => true, :length => { :minimum => 10 }
  validates :conteudo, :presence => true, :length => { :minimum => 10 }
end
