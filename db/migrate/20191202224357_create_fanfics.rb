class CreateFanfics < ActiveRecord::Migration[6.0]
  def change
    create_table :fanfics do |t|
      t.string :titulo
      t.string :genero
      t.string :status
      t.text :resumo
      t.text :conteudo

      t.timestamps
    end
  end
end
